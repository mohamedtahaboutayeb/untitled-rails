Rails.application.routes.draw do



  post '/rate' => 'rater#create', :as => 'rate'
  get 'main/index'
  get 'dashboards/seller'
  get 'dashboards/client'

  resources :products do
    resources :reservations , only: [:new,:create, :destroy]
    resources :comments

      member do
        put 'like', to: "products#like"
        put 'unlike', to: "products#unlike"
        get 'show-disponibility', to: "products#disponible"
      end

  end

  resources :notifications do

      collection do
        post :mark_as_read
      end

  end
  devise_for :users,path: '', path_names: { sign_in: 'login', sign_up: 'auth/Connect',sign_out: 'logout', edit: 'u/modify'}, controllers: {
        sessions: 'users/sessions',
        registrations: 'users/registrations',
        confirmations: 'users/confirmations',
        passwords: 'users/passwords',
        omniauth_callbacks: 'users/omniauth_callbacks',
        invitations: 'users/invitations'

      }

  root 'products#index'
  match "*path", to: "application#index", via: :all
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
