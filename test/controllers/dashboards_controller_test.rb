require 'test_helper'

class DashboardsControllerTest < ActionDispatch::IntegrationTest
  test "should get seller" do
    get dashboards_seller_url
    assert_response :success
  end

  test "should get client" do
    get dashboards_client_url
    assert_response :success
  end

end
