json.extract! reservation, :id, :dateA, :duration, :dateB, :user_id, :product_id, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
