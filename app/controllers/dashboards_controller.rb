class DashboardsController < ApplicationController
  before_action :authenticate_user!
  def seller
    @products = Product.where(traiteur_id:  current_user.id)
    @reservations = Reservation.all
    authorize! :seller,:seller

  end
  def client
    #@reservations = current_user.reservations.where(user_id: current_user)
    authorize! :client,:client
  end
end
