class ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:create,:destroy]

  # GET /reservations
  # GET /reservations.json
  def index
    @reservations = Reservation.all
  end

  # GET /reservations/1
  # GET /reservations/1.json
  def show

  end
  # GET /reservations/new
  def new

    @product = Product.find(params[:product_id])
    @reservation = @product.reservations.build
    @reservation.client_id = current_user.id
  end
  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  # POST /reservations.json
  def create
    @product = Product.find(params[:product_id])
    @reservation = @product.reservations.new(reservation_params)
    @reservation.client_id = current_user.id if current_user
    @product.reserved_at = Time.zone.now



      respond_to do |format|

        if @reservation.save
          @product.is_borrowed = true
          @product.status = 0
            #create the notifications
            Notification.create(recipient: @product.traiteur, actor: current_user, action: "reserved", notifiable: @reservation,product: @product)
        format.html { redirect_to @product, notice: 'la réservation a été bien effectuée .' }
        format.json { render :show, status: :created, location: @reservation }
        else
        format.html { render 'reservation_errors'}
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
        end
      end
  end

  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.html { redirect_to @reservation, notice: 'Reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation }

      else
        format.html { render :edit }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }

      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy

    @reservation = Reservation.find(params[:id])
    @reservation.destroy
    respond_to do |format|
      format.html { redirect_to @reservation, notice: 'Reservation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_params
      params.require(:reservation).permit(:dateA, :dateB,:product_id,:client_id,:nombres_invites,:TimeA,:TimeB)
    end
end
