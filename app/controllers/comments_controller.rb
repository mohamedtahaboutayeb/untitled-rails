class CommentsController < ApplicationController
  # a user must be logged in to comment
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :set_product
  def create

    # finds the product with the associated product_id
    @product = Product.find(params[:product_id])
    # creates the comment on the product passing in params
    @comment = @product.comments.new(comment_params)


    # assigns logged in user's ID to
    @comment.user = current_user if current_user

    # saves it

    if @comment.save
    #create the notifications
      (@product.users.uniq - [current_user]).each do |user|
        Notification.create(recipient: user, actor: current_user, action: "posted", notifiable: @comment,product: @product)
      end
      redirect_to @product
    else
      redirect_to @product

    end

  end


  def destroy
    @product = Product.find(params[:product_id])
    @comment = @product.comments.find(params[:id])
    @comment.destroy
    redirect_to product_path(@product)
  end

  private
  def set_product
    @product = Product.find(params[:product_id])
  end
  def comment_params
    params.require(:comment).permit(:name, :response)
  end
end
