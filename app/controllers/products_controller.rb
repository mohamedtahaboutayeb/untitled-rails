class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy, :like, :unlike,:disponible]
  before_action :authenticate_user!, only: [:edit, :update, :destroy, :like, :unlike]
  impressionist actions: [:show], unique: [:impressionable_type, :impressionable_id, :session_hash]
  include Pagy::Backend


  # GET /products
  # GET /products.json
  def index
    #@products = Product.all
     @q = Product.ransack(params[:q])
     @products = @q.result.includes(:traiteur,:comments)
      @pagy,@products =pagy(@products,items: 4)


  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id])
    @reservations = @product.reservations.all
  end

  # GET /products/new
  def new
    @product = Product.new
    @product.traiteur_id = current_user.id
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.create(product_params)
    @product.traiteur_id = current_user.id

    respond_to do |format|
      if @product.save
        @product.is_borrowed = true
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end

  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy

    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  def like
    @product.liked_by current_user
    #create notification
    Notification.create(recipient: @product.traiteur, actor: current_user, action: "liked", notifiable: @product,product: @product)
    respond_to do |format|
      format.html { redirect_back fallback_location: root_path }
      format.json { render layout:false }
    end
  end

  def unlike
    @product.unliked_by current_user
    respond_to do |format|
      format.html { redirect_back fallback_location: root_path }
      format.json { render layout:false }
    end
  end
  def disponible

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:title, :description, :price,:traiteur_id,:product_photo,:category,more_uploads: [])
    end
end
