require "application_responder"

class ApplicationController < ActionController::Base

  self.responder = ApplicationResponder
  respond_to :html

  include Pagy::Backend
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :adjust_format_for_iphone
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url , :error => exception.message
  end
  def index
    flash.notice = 'cette adresse est introuvable :/'
    redirect_to root_path
  end

  # The path used after sign in.
   def after_sign_in_path_for(resource)
     if current_user.is_seller?
       dashboards_seller_path
     else
       dashboards_client_path
     end
   end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:accept_invitation, keys: [:name,:is_seller])
  end

  private
  def adjust_format_for_iphone
    request.format = :ios if request.env["HTTP_USER_AGENT"] =~ %r{Mobile/.+Safari}
  end

end
