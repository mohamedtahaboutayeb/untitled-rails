class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise  :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable,:invitable, :omniauthable, omniauth_providers: %i[facebook], :invite_for => 2.weeks

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name   # assuming the user model has a name
      user.avatar_url = auth.info.image # assuming the user model has an image
      # If you are using confirmable and the provider(s) you use validate emails,
      # uncomment the line below to skip the confirmation emails.
       user.skip_confirmation!
    end
  end

  #def self.new_with_session(params, session)
    #super.tap do |user|
      #if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
       # user.email = data["email"] if user.email.blank?
     # end
    #end
  #end
  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end
  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end


  has_many :products , dependent: :destroy, foreign_key: :traiteur_id
  has_many :reservations,dependent: :destroy,foreign_key: :client_id
  has_many :products ,class_name: 'Product',foreign_key: :client_id,through: :reservations
  has_many :comments, dependent: :destroy
  has_many :notifications, foreign_key: :recipient_id ,dependent: :destroy
  has_many :sent_notifications, class_name: 'Notification', foreign_key: :actor_id, dependent: :destroy
  acts_as_voter
  validates :name , presence: true
   validate :password_complexity
  def password_complexity
    # Regexp extracted from https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a
    return if password.blank? || password =~ /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,70}$/

    errors.add :password, 'Complexity requirement not met. Length should be 8-70 characters and include: 1 uppercase, 1 lowercase, 1 digit and 1 special character'
  end
  def is_seller?()
     self.is_seller == true


  end
  def is_client?()
      self.is_seller == false
  end

end
