class Comment < ApplicationRecord
  belongs_to :product
  belongs_to :user

  validates :response , length: { maximum: 200,
                           too_long: "%{count} caractéres en maximum" }
  validates :response , length: { minimum: 10,
                                  too_short: "%{count} caractéres en minimum" }
  validates :response , presence: true

end
