class Reservation < ApplicationRecord
  belongs_to :product
  belongs_to :client , class_name: 'User',foreign_key:  :client_id

  #validations
  validates :dateA ,presence: true
  validates :dateB ,presence: true

  validates_datetime :dateB, :on_or_after => :dateA # Method symbol

  validates_date :dateA, :on => :create, :on_or_after => :today ,:on_or_after_message => "veuillez entrer une date valide !"
  validates :nombres_invites, :numericality => { :greater_than_or_equal_to => 5 , :less_than_or_equal_to => 200 }

  accepts_nested_attributes_for :product
  def start_time
    self.dateA
  end
  def end_time
    self.dateB
  end
end
