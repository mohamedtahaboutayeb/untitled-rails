class Ability
  include CanCan::Ability

  def initialize(user)

    can :seller , :seller if user.is_seller?
    can :client, :client if user.is_client?

    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
