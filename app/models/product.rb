class Product < ApplicationRecord
  mount_uploader :product_photo, ProductPhotoUploader


  belongs_to :traiteur, class_name: 'User', foreign_key: :traiteur_id
  has_many :comments, dependent: :destroy # if a product is deleted so are all of its comments
  has_many :reservations, dependent: :destroy
  has_many :clients , class_name: 'User',foreign_key: :product_id,:through => :reservations
  has_many :users , through: :comments
  has_many_attached :more_uploads
  has_many :notifications, foreign_key: :product_id ,dependent: :destroy


  enum status: [valable: 0  , non_valable: 1]
  #other
  acts_as_votable
  is_impressionable


  #validations
  validates :title , presence: true
  validates :description , presence: true

  validates :description, length: { maximum: 250, too_long: "%{count} caractéres en maximum" }
  validates :product_photo , presence: true
  enum category: [:Table , :Buffet ,:autres ]

  #Scopes

  #Nested attributes
  accepts_nested_attributes_for :reservations

end
