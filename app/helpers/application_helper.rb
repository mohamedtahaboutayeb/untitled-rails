module ApplicationHelper
  #pagy
  include Pagy::Frontend

  def bootstrap_class_for(flash_type)
    {
        success: "alert-success",
        error: "alert-danger",
        alert: "alert-warning",
        notice: "alert-info"
    }.stringify_keys[flash_type.to_s] || flash_type.to_s
  end
  #gravatar url
  def avatar_url(user)
    if user.avatar_url.present?
      user.avatar_url
    else
      default_url = "https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y"
      gravatar_id = Digest::MD5.hexdigest(user.email).downcase
      "http://gravatar.com/avatar/#{gravatar_id}.png?s=48&d=#{CGI.escape(default_url)}"
    end
  end


end
