source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.1'
# Use mysql as the database for Active Record
gem 'mysql2', '>= 0.4.4', '< 0.6.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
 gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
#gem 'therubyracer', '~> 0.12.3'
gem 'mini_racer', '~> 0.2.4'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'bootstrap-sass', '~> 3.3', '>= 3.3.7'
gem 'jquery-rails'
gem 'jquery-ui-rails', '~> 6.0', '>= 6.0.1'
gem 'twitter-bootstrap-rails', :git => 'git://github.com/seyhunak/twitter-bootstrap-rails.git'
gem 'simple_form', '~> 4.0', '>= 4.0.1'
gem 'carrierwave', '~> 1.2', '>= 1.2.3'
gem 'devise'
gem 'devise_invitable', '~> 1.7.0'
gem 'autoprefixer-rails', '~> 9.3', '>= 9.3.1'
gem 'omniauth-facebook'
gem 'font-awesome-sass', '~> 5.4', '>= 5.4.1'
gem 'impressionist', '~> 1.6'
gem 'acts_as_votable', '~> 0.11.1'
gem 'ransack'
gem 'bootstrap-datepicker-rails'
gem 'momentjs-rails'
gem 'strong_password', '~> 0.0.6'
gem 'devise-security'
gem 'pagy', '~> 0.22.0'
gem 'webpacker', '~> 3.5'
gem 'responders'
gem 'selectize-rails'
gem 'stateful_enum'
gem "simple_calendar", "~> 2.0"
gem 'cancancan', '~> 1.9'
gem 'validates_timeliness', '~> 5.0.0.alpha3'
gem 'client_side_validations'
gem 'client_side_validations-simple_form'
gem 'jquery-minicolors-rails'
gem 'pickdate'
group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
