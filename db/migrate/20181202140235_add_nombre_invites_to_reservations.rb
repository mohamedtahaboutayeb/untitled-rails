class AddNombreInvitesToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :nombres_invites, :integer
  end
end
