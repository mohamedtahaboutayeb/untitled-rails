class AddTimeBToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :TimeB, :date
  end
end
