class AddProductPhotoToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :product_photo, :string
  end
end
