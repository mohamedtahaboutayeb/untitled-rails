class AddIsBorrowedToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :is_borrowed, :boolean
  end
end
