class AddReservedAtToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :reserved_at, :date
  end
end
