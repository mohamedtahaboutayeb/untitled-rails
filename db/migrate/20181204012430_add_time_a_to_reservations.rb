class AddTimeAToReservations < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :TimeA, :date
  end
end
