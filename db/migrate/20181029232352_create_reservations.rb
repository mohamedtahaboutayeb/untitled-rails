class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.datetime :dateA
      t.integer :duration
      t.datetime :dateB
      t.belongs_to :client, index: true
      t.belongs_to :product, index: true

      t.timestamps
    end
  end
end
